#ifndef SOME_TOOL
#define SOME_TOOL

// this is just an example tool, in general all tools you write should
// go in separate files


float getANumber();

#endif // SOME_TOOL
